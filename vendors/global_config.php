<?php 
	class global_config
	{
		private $action;
		private $enviroment;
		private $production_urls;
		public $is_production;
		
		function __construct()
		{
			$this->enviroment = $_SERVER['SERVER_NAME'];

			$this->production_urls = array(
					'pandora.pipocket.com' => true
				);

			$this->is_production = array_key_exists( $this->enviroment, $this->production_urls );
		}

		public function set_database()
		{
			switch( $this->enviroment )
			{
				case 'pandora.dev.pipocket.com':
					APP_DATABASE::$set = 'local';
					break;

				case 'pandora.pipocket.com':
					APP_DATABASE::$set = 'production';
					break;
			}
		}

		public function get_site_info()
		{
			$site_info = array();

			switch( $this->enviroment )
			{
				case 'pandora.dev.pipocket.com':
					$site_info['SITE_INFO_DOMAIN']	= 'dev.pipocket.com';
					$site_info['SITE_INFO_URL']		= 'http://' . $site_info['SITE_INFO_DOMAIN'];
					break;

				case 'pandora.pipocket.com':
					$site_info['SITE_INFO_DOMAIN']	= 'pipocket.com';
					$site_info['SITE_INFO_URL']		= 'http://' . $site_info['SITE_INFO_DOMAIN'];
					break;
			}

			return $site_info;
		}

		public function set_upload( $media_only = false )
		{
			$upload = array();

			switch ( $this->enviroment ) {
				case 'pandora.dev.pipocket.com':
					if ( !$media_only )
					$upload['UPLOAD_PATH'] 		= dirname( AURA_ROOT ) . DS . 'Media-files';

					$upload['MEDIA_FILES_URL']  = 'http://media-files.dev.pipocket.com';
					break;
				
				default:
					if ( !$media_only )
					$upload['UPLOAD_PATH'] 		= dirname( AURA_ROOT ) . DS . 'temporary-mediafiles';

					$upload['MEDIA_FILES_URL']  = 'http://media-files.pipocket.com';
					break;
			}

			return $upload;
		}
	}
?>