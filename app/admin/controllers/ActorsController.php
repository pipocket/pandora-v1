<?php
    class ActorsController extends AppController
	{
		public function index()
		{
			$this->show('index');
		}

		public function all()
		{
			$actors = $this->Actor->all();
			$this->ajax( $actors );
		}
	}
?>