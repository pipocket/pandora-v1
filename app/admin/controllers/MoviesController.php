<?php
    class MoviesController extends AppController
	{
		public $area = 'movie';

		public function __construct()
		{
			parent::__construct();
			$this->set( 'menu_area', 'movie' );
		}

		public function index()
		{
			$this->session( 'current_filter', $_SERVER['REQUEST_URI'] );
			
			$this->set( 'movies', $this->Movie->all( $this->pagination() ) );
			$this->set( 'movie_status', $this->get_message_and_kill( 'movie_status' ) );
			$this->set( 'movie_message', $this->get_message_and_kill( 'movie_message' ) );
			$this->show( 'index' );
		}

		public function edit( $id )
		{
			$this->set( 'movie', $this->Movie->one( $id ) );
			$this->show( 'edit' );
		}

		public function add()
		{	
			$this->show( 'edit' );
		}

		public function save()
		{
			$result = $this->Movie->save();
			$this->message( 'movie_status', $result->status ? 'success' : 'error' );

			if ( $result->status AND !$result->update ) {
				$this->message( 'movie_message', "<strong>Pronto!</strong> <a href=' " . $this->path( 'approot' ) . "/movies/edit/{$result->movie_id}'>{$result->movie_name}</a> foi cadastrado com sucesso." );
			}
			elseif ( $result->status )
				$this->message( 'movie_message', "<strong>Pronto!</strong> <a href=' " . $this->path( 'approot' ) . "/movies/edit/{$result->movie_id}'>{$result->movie_name}</a> foi atualizado com sucesso." );
			else
				$this->message( 'movie_message', "<strong>Xii!</strong> Ocorreu um erro ao tentar salvar {$result->movie_name}, tente novamente mais tarde :(" );

			if ( $this->session( 'current_filter') )
				$this->redirect( $this->session( 'current_filter') );
			else
				$this->redirect( 'movies', 'admin' );
		}

		public function key( $key, $movie_id = null )
		{
			$result = $this->Movie->key( $key, $movie_id );
			$this->ajax( $result );
		}

		public function remove()
		{
			$result = $this->Movie->remove();
			$this->ajax( $result );
		}

		public function load_preview_data()
		{
			// Cinemark - http://www.cinemark.net.br/filmes/proximos-lancamentos
			$queryUrl = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D%22http%3A%2F%2Fwww.cinemark.net.br%2Ffilmes%2Fproximos-lancamentos%22%20AND%20xpath%20%3D%20'%2F%2Fdiv%5B%40id%3D%22accordion%22%5D'&format=json&callback=";

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $queryUrl); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$output = curl_exec($ch); 
			curl_close($ch);

			header('Cache-Control: no-cache, must-revalidate');
			header('Content-type: application/json');
			echo $output;
		}

		public function upload( $type )
		{
			switch ( $type ) {
				case 'banner':
					$options = array( 'dir' => '/movies', 'resize' => array( '934', '184' ) );
					$upload = $this->vendors( 'upload' );
					$result = $upload->send( $options );
					break;

				case 'thumb1':
					$options = array( 'dir' => '/movies', 'resize' => array( '270', '97' ) );
					$upload = $this->vendors( 'upload' );
					$result = $upload->send( $options );
					break;

				case 'thumb2':
					$options = array( 'dir' => '/movies', 'resize' => array( '108', '108' )  );
					$upload = $this->vendors( 'upload' );
					$result = $upload->send( $options );
					break;

				case 'poster':
					$options = array( 'dir' => '/movies', 'resize' => array( '123', '174' )  );
					$upload = $this->vendors( 'upload' );
					$result = $upload->send( $options );
					break;

				case 'facebook':
					$options = array( 'dir' => '/movies', 'resize' => array( '90', '90' )  );
					$upload = $this->vendors( 'upload' );
					$result = $upload->send( $options );
					break;

				default:
					return false;
					break;
			}

			echo $result->name;
		}
	}
?>