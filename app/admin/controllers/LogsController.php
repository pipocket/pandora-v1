<?php
    class LogsController extends AppController
	{
		public $area = 'logs';

		public function __construct()
		{
			parent::__construct();
			$this->set( 'menu_area', 'logs' );
		}

		public function index()
		{
			require AURA_ROOT . DS . 'vendors' . DS . 'Github' . DS . 'Autoloader.php';
			Github_Autoloader::register();
			$github = new Github_Client();
			
			$github->authenticate('PauloMartins', 'plains06', Github_Client::AUTH_HTTP_PASSWORD);
			$commits->site = $github->getCommitApi()->getBranchCommits('PauloMartins', 'Pipocket', '');
			$commits->admin = $github->getCommitApi()->getBranchCommits('PauloMartins', 'Pipocket-Administration', '');

			$this->set( 'commits', $commits );
			$this->show( 'index' );
		}
	}
?>