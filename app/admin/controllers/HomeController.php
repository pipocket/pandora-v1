<?php
    class HomeController extends AppController
	{
		public $area = 'home';

		public function __construct()
		{
			parent::__construct();
			$this->set( 'menu_area', 'home' );
		}

		public function index()
		{
			$this->set( 'last_users', $this->User->last( 'name,register', 'register' ) );
			$this->set( 'last_messages', $this->Feedback->last( 'comment, date') );
			$this->set( 'last_movies', $this->Movie->last( 'name, date' ) );
			$this->show( 'index' );
		}
	}
?>