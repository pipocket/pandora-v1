<?php
    class DirectorsController extends AppController
	{
		public function index()
		{
			$this->show('index');
		}

		public function all()
		{
			$diretors = $this->Director->all();
			$this->ajax( $diretors );
		}
	}
?>