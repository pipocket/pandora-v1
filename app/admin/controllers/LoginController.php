<?php
    class LoginController extends AppController
	{
		function __construct()
		{
			parent::__construct( false );
		}

		public function index()
		{
			$url_target = $this->session( 'url_target' );
			$url_target = $url_target ? $url_target : $this->message( 'url_target');

			$this->session( 'destroy' );
			$this->set( 'error_login', $this->message( 'error_login') );
			$this->set( 'email_login', $this->message( 'email_login') );
			$this->set( 'url_target', $url_target );
			$this->show( 'index', false );
		}

		public function in()
		{
			$result = $this->User->login();
		}

		public function out()
		{
			$this->session( 'destroy' );
			$this->redirect( '/' );
		}
	}
?>