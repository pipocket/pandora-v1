<?php
    class FeedbackController extends AppController
	{
		public $area = 'feedback';

		public function __construct()
		{
			parent::__construct();
			$this->set( 'menu_area', 'feedback' );
		}

		public function index()
		{
			$this->set( 'feedback', $this->Feedback->all( $this->pagination() ) );
			$this->show('index');
		}
	}
?>