<?php
    class AppController extends aura_controller
	{
		public function __construct( $verify = true )
		{
			parent::__construct();

			if ( $verify ) {
				if ( !$this->security() ) {
					$this->session( 'url_target', $_SERVER['REQUEST_URI'] );
					$this->redirect( '/login' );
					exit;
				}
				else {
					$this->check_area();
					$this->set( 'menu', $this->menu() );
					$this->set( 'usertime', $this->session( 'usertime' ) );
					$this->set( 'username', base64_decode( $this->session( 'username' ) ) );
					$this->set( 'userpermission', $this->permission() );
				}
			}
		}

		private function menu()
		{
			$menu = array(
					'home' => array( 'label'=>'Home', 'url'=>'/home' ),
					'movie' => array( 'label'=>'Filmes', 'url'=>'/movies' ),
					'feedback' => array( 'label'=>'Feedback', 'url'=>'/feedback' ),
					'logs' => array( 'label'=>'Logs', 'url'=>'/logs' )
				);

			$areas = $this->permissions();

			if ( $areas == 'none' )
				$menu = array();
			else if ( is_array( $areas ) ) {
				foreach ( $menu as $item => $data ) {
					if ( !array_key_exists( $item, $areas) )
						unset( $menu[$item] );
				}
			}

			return $menu;
		}

		private function check_area()
		{
			$areas = $this->permissions();
			
			if ( is_array($areas) AND !array_key_exists( $this->area, $areas) )
				$this->redirect( 'home' );	
		}

		private function permission()
		{
			return base64_decode( $this->session( 'user_admin_permission' ) );
		}

		private function permissions()
		{
			$permission = $this->permission();
			
			switch ( $permission )
			{
				case 1:
					$area = 'all';
					break;
				
				case 2:
					$area = array( 'home'=>true, 'movie'=>true );
					break;

				default:
					$area = 'none';
					break;
			}

			return $area;
		}

		public function security()
		{
			if ( $this->session( 'userid' ) AND $this->session( 'usertime' ) ) {
				$now = mktime();
				$usertime = strtotime($this->session( 'usertime' ));

				return ( $now < $usertime );
			}

			return false;
		}

		public function ajax( $data )
		{
			if ( Helper::is_ajax_request() ) {
				$method = isset( $_GET['method'] ) ? $_GET['method'] : 'json';

				switch ( $method ) {
				 	case 'json':
				 		$data = isset( $data['data'] ) ? $data['data'] : $data;
				 		header('Cache-Control: no-cache, must-revalidate');
						header('Content-type: application/json');

				 		echo json_encode( $data );
				 		break;
				 	
				 	case 'array':
				 		$items = '';
				 		$fields = explode( ',', $_GET['fields'] );

				 		foreach ( $data['data'] as $item ) {
				 			$items .= "\"";

				 			foreach ( $fields as $field ) {
				 				$items .= "{$item->$field}#";
				 			}

				 			$items = substr( $items, 0, -1 );
				 			$items .= "\",";
				 		}

				 		echo substr( $items, 0, -1 );
				 		break;
				 }
			}
		}

		public function get_message_and_kill( $name )
		{
			$var = $this->message( $name );
			$this->message( $name, null );
			
			return $var;
		}

		public function pagination()
		{
			$page = isset( $_GET['page'] ) ? $_GET['page'] : 1;
			$pagination = array( 'page' => $page, 'registers' => 20 );
			return $pagination;
		}
	}
?>