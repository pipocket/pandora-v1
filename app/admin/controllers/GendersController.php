<?php
    class GendersController extends AppController
	{
		public function index()
		{
			$this->show('index');
		}

		public function all()
		{
			$genders = $this->Gender->all();
			$this->ajax( $genders );
		}
	}
?>