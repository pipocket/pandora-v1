<?php
	// Call global config
	include AURA_ROOT . DS . 'vendors' . DS . 'global_config.php';
	$global_config = new global_config( 'set_api_keys' );

	// Get vars
	$site_info = $global_config->get_site_info();
	$upload = $global_config->set_upload();

	// Defines default controller in app
	define('AURA_DEFAULT_CONTROLLER', 'home');
	// Defines default action in app
	define('AURA_DEFAULT_ACTION', 'index');
	
	// Defines SMTP configuration
	define('AURA_SMTP_HOST', 'smtp.gmail.com');
	define('AURA_SMTP_AUTH', true); 	 // (bolean) smtp authentication
	define('AURA_SMTP_LANG', 'br'); 
	define('AURA_SMTP_USERNAME', 'noreply@pipocket.com'); // email
	define('AURA_SMTP_PASSWORD', 'noreply#pppckt');
	define('AURA_SMTP_PORT', '465');
	define('AURA_SMTP_FROMNAME', 'Pipocket - Pandora'); // title message

	// Activation mail
	define('ACTIVATION_MAIL', 'noreply@pipocket.com');
	
	// Defines folder for master pages
	define('AURA_SMARTY_MASTERPAGES_FOLDER', '_MasterPages');
	define('AURA_SMARTY_MASTERPAGE_DEFAULT', 'master');
	
	// Token to be added to md5
	define('AURA_MD5_TOKEN', '@#$%^67dfs&*()[');

	define('PROJECT_NAME', 'Pipocket');

	// Site info
	foreach( $site_info as $key => $value )
		define($key, $value);

	// Upload
	foreach( $upload as $key => $value )
		define($key, $value);
?>