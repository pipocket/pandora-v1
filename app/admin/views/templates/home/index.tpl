{block name="body"}
<div class="row">
  <div class="span12">
    <ul class="breadcrumb">
    	<li>Home</li>
    </ul>
  </div>
</div>

<div class="space4"></div>

<div class="row">
	<div class="span4">
		<h3>Filmes - Últimos adicionados</h3>
		<div class="space1"></div>
		<ul class="nav nav-tabs nav-stacked">
			{foreach from=$last_movies item=last_movie}
			<li><a href="{$aura->root}/movies/edit/{$last_movie->id}">{$last_movie->name} <small class="info normal">{$last_movie->date|date_format:'%d/%m/%Y %H:%M'}</small></a></li>
			{/foreach}
		</ul>
	</div>

	<div class="span4">
		<h3>Usuários - Últimos registros</h3>
		<div class="space1"></div>
		<ul class="nav nav-tabs nav-stacked">
			{foreach from=$last_users item=user}
			<li class="active">
				<a href="#">{$user->name} <small class="info normal">{$user->register|date_format:'%d/%m/%Y %H:%M'}</small>
				</a>
			</li>
			{/foreach}
		</ul>
	</div>
	<div class="span4">
		<h3>Fale conosco - Últimas mensagens</h3>
		<div class="space1"></div>
		<ul class="nav nav-tabs nav-stacked">
			{foreach from=$last_messages item=message}
			<li class="active">
				<a href="#" title="{$message->comment}">{$message->comment|substr:0:50}... 
				<small class="info normal">{$message->date|date_format:'%d/%m/%Y %H:%M'}</small>
				</a>
			</li>
			{foreachelse}
			<li class="active"><a href="#">Nenhum</a></li>
			{/foreach}
		</ul>
	</div>
</div>
{/block}