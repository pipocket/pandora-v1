<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta charset="utf-8">
  <title>{$smarty.const.PROJECT_NAME} - Administração</title>
  
  <link rel="stylesheet" type="text/css" href="{$html->css}/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="{$html->css}/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="{$html->css}/jquery-ui-1.8.18.custom.css">
  <link rel="stylesheet" type="text/css" href="{$html->css}/admin.css">
  <script type="text/javascript" src="{$html->js}/libs/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="{$html->js}/plugins/jquery.maskedinput.min.js"></script>
  <script type="text/javascript" src="{$html->js}/admin.js"></script>
  <script type="text/javascript" src="{$html->js}/validation.js"></script>
  <script type="text/javascript" src="{$html->js}/plugins/fileuploader.js"></script>
  <script type="text/javascript" src="{$html->js}/plugins/bootstrap.min.js"></script>
  <script type="text/javascript" src="{$html->js}/plugins/bootstrap-typeahead.js"></script>
  <script type="text/javascript" src="{$html->js}/plugins/jquery-ui-1.8.18.custom.min.js"></script>
  <script type="text/javascript" src="{$aura->js}"></script>
  {$aura->jsvars}
</head>

<body>
  <div class="navbar">
    <div class="navbar-inner">
      <div class="container">
        
        <a class="brand fl" href="http://{$smarty.const.SITE_INFO_DOMAIN}">{$smarty.const.PROJECT_NAME}</a>

        <ul class="nav">
          {foreach from=$menu item=item key=name}
          <li {if $menu_area == $name}class="active"{/if}>
            <a href="{$aura->root}{$item.url}">{$item.label}</a>
          </li>
          {/foreach}
        </ul>

        <ul class="nav pull-right">
          <li><a href="#">Você permanecerá logado até: {$usertime|date_format:'d/m/Y á\s H\hi'}</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$username}<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="{$aura->root}/login/out">Sair</a></li>
            </ul>
          </li>
        </ul>

      </div>
    </div>
  </div>
    
  <div class="container">
    {block name="body"}{/block}

    <div class="footer">
      <p>© 2012 {$smarty.const.PROJECT_NAME}</p>
    </div>
  </div>

{block name="call-script"}{/block}
<script type="text/javascript">
    $(function(){
      {block name="script"}{/block}
    });
</script>
</body>
</html>