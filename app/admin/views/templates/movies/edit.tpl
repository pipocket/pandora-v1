{block name="body"}
<div class="row">
  <div class="span12">
    <ul class="breadcrumb">
    	<li><a href="{$aura->root}/home">Home</a> <span class="divider">/</span></li>
		<li>
			<a href="{$aura->root}/movies">Filmes</a> <span class="divider">/</span>
		</li>
		<li>
			{if isset($movie)}
				Editar<span class="divider">/</span>
				{$movie->name}
			{else}
				Adicionar
			{/if}
		</li>
    </ul>
  </div>
</div>

<div class="space4"></div>

<div class="row">
	<div class="span12">
		<h2>{if isset($movie)}EDITAR{else}ADICIONAR{/if} FILME</h2>

		<form action="{$aura->root}/movies/save" name="movie" method="post" class="form-horizontal" data-validate="true">
			<fieldset>
				<div class="space1"></div>

				<ul class="nav nav-tabs tabs">
					<li class="active">
						<a href="#basic" data-toggle="tab">Dados básicos</a>
					</li>
					<li>
						<a href="#media" data-toggle="tab">Imagens</a>
					</li>
				</ul>

				<div class="space1"></div>

				<div class="tab-content">
					<div class="tab-pane active" id="basic">
						<div class="container-alert-message"></div>

						<div class="row">
							<div class="span8">
								<div class="control-group">
									<label for="" class="control-label">Título</label>
									<div class="controls">
										<input type="text" name="name" class="input-xlarge span5" value="{$movie->name|default:''}" data-required="true" data-message="Preencha o campo Título" />
										<input type="button" class="btn btn-info" id="load-imdb-data" value="IMDB">
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Título original</label>
									<div class="controls">
										<input type="text" name="original_name" class="input-xlarge span5" value="{$movie->original_name|default:''}" data-required="true" data-message="Preencha o campo Título original" />
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Url</label>
									<div class="controls input-prepend">
										<span class="add-on"><i class="icon-share"></i></span>
										<input type="text" name="key" class="input-xlarge span3" readonly="true" value="{$movie->key|default:''}" data-required="true" data-message="O campo Url não foi gerado, verifique o título do filme" />
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Pré estréia</label>
									<div class="controls input-prepend">
										<span class="add-on"><i class="icon-calendar"></i></span>
										<input type="text" name="preview" class="input-xlarge span2 datepicker" autocomplete="off" value="{$movie->preview|default:''}" />
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Estréia</label>
									<div class="controls input-prepend">
										<span class="add-on"><i class="icon-calendar"></i></span>
										<input type="text" name="release" class="input-xlarge span2 datepicker" autocomplete="off" value="{$movie->release|default:''}" data-required="true" data-message="Preencha o campo Estréia" /> <input type="button" class="btn btn-info" id="load-movie-previews" value="Cinemark">
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Classificação etária</label>
									<div class="controls input-prepend">
										<span class="add-on"><i class="icon-eye-open"></i></span>
										<input type="text" name="age" class="input-xlarge span1" value="{$movie->age|default:''}" data-required="true" data-message="Preencha o campo Classificação etária" /> <span class="help-inline"><strong>L</strong> para classificação livre</span>
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Duração</label>
									<div class="controls input-prepend">
										<span class="add-on"><i class="icon-time"></i></span>
										<input type="text" name="time" class="input-xlarge span1" maxlength="3" value="{$movie->time|default:''}" data-required="true" data-message="Preencha o campo Duração" data-accept-value="0" /> <span class="help-inline">Em <strong>minutos</strong> e <strong>apenas números</strong></span>
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Ano</label>
									<div class="controls">
										<select name="year" class="span1">
											<option value="2007" {if isset($movie) AND $movie->year == '2007'}selected="selected"{/if}>2007</option>
											<option value="2008" {if isset($movie) AND $movie->year == '2008'}selected="selected"{/if}>2008</option>
											<option value="2009" {if isset($movie) AND $movie->year == '2009'}selected="selected"{/if}>2009</option>
											<option value="2010" {if isset($movie) AND $movie->year == '2010'}selected="selected"{/if}>2010</option>
											<option value="2011" {if isset($movie) AND $movie->year == '2011'}selected="selected"{/if}>2011</option>
											<option value="2012" {if (isset($movie) AND $movie->year == '2012') OR !isset($movie)}selected="selected"{/if}>2012</option>
											<option value="2013" {if isset($movie) AND $movie->year == '2013'}selected="selected"{/if}>2013</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">País</label>
									<div class="controls input-prepend">
										<span class="add-on"><i class="icon-map-marker"></i></span>
										<input type="text" name="origin" class="input-xlarge span1" value="{$movie->origin|default:''}" data-required="true" data-message="Preencha o campo Paíss" /> <span class="help-inline">Sigla de 3 dígitos</span>
									</div>
								</div>
								<div class="control-group">
									<label for="" class="control-label">Sinopse</label>
									<div class="controls">
										<textarea name="storyline" class="span5" rows="10" data-required="true" data-message="Preencha o campo Sinopse" >{$movie->storyline|default:''}</textarea>
									</div>
								</div>
								{if $userpermission == 1}
								<div class="control-group">
									<label for="" class="control-label"><strong>Sinopse revisada?</strong></label>
									<div class="controls">
										<input type="radio" name="storyline_check" value="1" {if isset($movie->storyline_check) AND $movie->storyline_check == 1}checked="true"{/if} /> Sim &nbsp;&nbsp;
										<input type="radio" name="storyline_check" value="0" {if !isset($movie->storyline_check) OR $movie->storyline_check == 0}checked="true"{/if}/> Não
									</div>
								</div>
								{/if}
							</div>

							<div class="offset1 span3">
								{if $userpermission == 1}
								<div class="well">
									<label for="" class="checkbox">
										<input type="checkbox" name="publish" value="1" {if isset($movie) AND $movie->status == 1}checked="true"{/if}/> Publicar quando salvar
									</label>
								</div>
								{/if}

								<div class="well">
									<fieldset>
										<input type="text" class="span2 typeahead-gender" placeholder="Digite o(s) gênero(s)" data-type="genders" data-name="genders[]">
										<input type="button" class="btn" value="+" data-action="add-to-list" data-type="genders">

										<div class="space1"></div>
										<ul class="list" id="list_genders">
										{if isset($movie)}
											{foreach from=$movie->genders item=gender}
											<li><input type="hidden" name="genders[]" value="{$gender->gender_id}">{$gender->description}<a href="" class="close" rel='remove'>&times;</a></li>
											{/foreach}
										{/if}
										</ul>
									</fieldset>
								</div>

								<div class="well">
									<fieldset>
										<input type="text" class="span2 typeahead-director" placeholder="Digite o(s) diretor(es)" data-type="directors" data-name="directors[]">
										<input type="button" class="btn" value="+" data-action="add-to-list" data-type="directors">

										<div class="space1"></div>
										<ul class="list" id="list_directors">
										{if isset($movie)}
											{foreach from=$movie->directors item=director}
											<li><input type="hidden" name="directors[]" value="{$director->director_id}">{$director->name}<a href="" class="close" rel='remove'>&times;</a></li>
											{/foreach}
										{/if}
										</ul>
									</fieldset>
								</div>

								<div class="well">
									<fieldset>
										<input type="text" class="span2 typeahead-actor" placeholder="Digite os atores principais" data-type="actors" data-name="actors[]">
										<input type="button" class="btn" value="+" data-action="add-to-list" data-type="actors">

										<div class="space1"></div>
										<ul class="list" id="list_actors">
										{if isset($movie)}
											{foreach from=$movie->actors item=actor}
											<li><input type="hidden" name="actors[]" value="{$actor->actor_id}">{$actor->name}<a href="" class="close" rel='remove'>&times;</a></li>
											{/foreach}
										</ul>
										{/if}
									</fieldset>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="media">
						<div class="row">
							<div class="control-group">
								<label for="" class="control-label">Banner</label>
								<div class="controls">
									<a href="#" class="btn" id="uploadButton">Carregar imagem</a> <small class="info normal"> ( 934 x 184 )</small>
									<div class="box-image">
										<ul>
										{if isset($movie)}
											{foreach from=$movie->poster item=image}
											<li>
												<a href='#' class='btn btn-danger' rel='remove-image'>&times;</a><br/>
												<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$image->image}" alt="">
												<input type="hidden" name="banner[]" value="old#{$image->image}" />
											</li>
											{/foreach}
										{/if}
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="control-group">
								<label for="" class="control-label">Ranking</label>
								<div class="controls">
									<a href="#" class="btn" id="uploadButton2">Carregar imagem</a> <small class="info normal">( 270 x 97 )</small>
									<div class="box-image">
										<ul>
										{if isset($movie->thumbs) AND isset($movie->thumbs->thumb1) AND $movie->thumbs->thumb1}
											<li>
												<a href='#' class='btn btn-danger' rel='remove-image'>&times;</a><br/>
												<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$movie->thumbs->thumb1}" alt="">
												<input type="hidden" name="thumb1" value="old#{$movie->thumbs->thumb1}" />
											</li>
										{/if}
										</ul>
									</div>
								</div>
								</div>
						</div>

						<div class="row">
							<div class="control-group">
								<label for="" class="control-label">Thumb</label>
								<div class="controls">
									<a href="#" class="btn" id="uploadButton3">Carregar imagem</a> <small class="info normal">( 108 x 108 )</small>
									<div class="box-image">
										<ul>
										{if isset($movie->thumbs) AND isset($movie->thumbs->thumb2) AND $movie->thumbs->thumb2}
											<li>
												<a href='#' class='btn btn-danger' rel='remove-image'>&times;</a><br/>
												<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$movie->thumbs->thumb2}" alt="">
												<input type="hidden" name="thumb2" value="old#{$movie->thumbs->thumb2}" />
											</li>
										{/if}
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="control-group">
								<label for="" class="control-label">Poster</label>
								<div class="controls">
									<a href="#" class="btn" id="uploadButton4">Carregar imagem</a> <small class="info normal">( 123 x 174 )</small>
									<div class="box-image">
										<ul>
										{if isset($movie->thumbs) AND isset($movie->thumbs->poster) AND $movie->thumbs->poster}
											<li>
												<a href='#' class='btn btn-danger' rel='remove-image'>&times;</a><br/>
												<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$movie->thumbs->poster}" alt="">
												<input type="hidden" name="poster" value="old#{$movie->thumbs->poster}" />
											</li>
										{/if}
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="control-group">
								<label for="" class="control-label">Facebook</label>
								<div class="controls">
									<a href="#" class="btn" id="uploadButton5">Carregar imagem</a> <small class="info normal">( 90 x 90 )</small>
									<div class="box-image">
										<ul>
										{if isset($movie->thumbs) AND isset($movie->thumbs->facebook) AND $movie->thumbs->facebook}
											<li>
												<a href='#' class='btn btn-danger' rel='remove-image'>&times;</a><br/>
												<img src="{$smarty.const.MEDIA_FILES_URL}/movies/{$movie->thumbs->facebook}" alt="">
												<input type="hidden" name="facebook" value="old#{$movie->thumbs->facebook}" />
											</li>
										{/if}
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="span12 marker ar">
							<!--
							<input type="hidden" data-additional-elements-required="poster[],thumb1,thumb2" data-message="É necessário anexar as imagens relacionadas ao filme"/>
							-->

							{if $userpermission == 1}
								
									Este filme <strong>já pode ser revisado</strong>?&nbsp;
									<input type="radio" name="revision" value="1" id="revision" {if isset($movie) AND $movie->revision == 1}checked="true"{/if} /> Sim &nbsp;&nbsp;
									<input type="radio" name="revision" value="0" {if !isset($movie) OR $movie->revision == 0}checked="true"{/if}/> Não &nbsp;&nbsp;
									<input type="radio" name="revision" value="2" id="revision" {if isset($movie) AND $movie->revision == 2}checked="true"{/if} /> <strong>Já foi revisado</strong>
								&nbsp;&nbsp;&nbsp;&nbsp;
							{else}
								{if isset($movie) AND $movie->revision != 0}
									Este filme <strong>já foi revisado</strong>?&nbsp;
									<input type="radio" name="revision" value="2" id="revision" {if isset($movie) AND $movie->revision == 2}checked="true"{/if} /> Sim &nbsp;&nbsp;
									<input type="radio" name="revision" value="1" {if !isset($movie) OR $movie->revision == 1}checked="true"{/if}/> Não
									&nbsp;&nbsp;&nbsp;&nbsp;
								{else}
									<input type="hidden" name="revision" value="{$movie->revision}"/>
								{/if}

								<input type="hidden" name="publish" value="{$movie->status}"/>
								<input type="hidden" name="storyline_check" value="{$movie->storyline_check}"/>
							{/if}

							<input type="hidden" name="movie_id" value="{$movie->id|default:''}">
							<input type="hidden" name="revision_old" value="{$movie->revision|default:0}">
							<input type="button" class="btn btn-secundary" value="Cancelar"/>
							<input type="submit" class="btn btn-primary" value="Salvar"/>
							<span class="spaceH2"></span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

<!-- Imdb -->
<div class="modal fade" id="imdb">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Modal header</h3>
  </div>
  <div class="modal-body">
    <div class="clearfix modal-movie-info">
    </div>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn btn-info" rel="load-data">É este mesmo! Carregar dados disponíveis</a>
  </div>
</div>

<!-- Movie Preview data -->
<div class="modal fade" id="moviepreview">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Cinemark: Estréias oficiais</h3>
  </div>
  <div class="modal-body">
    <div class="modal-moviepreview-info">
    	<div class="filter"><input type="text" id="preview-filter" placeholder="Digite o nome do filme" /></div>
    	<ul></ul>
    </div>
  </div>
  <div class="modal-footer"></div>
</div>
{/block}

{block name="script"}
	$('input[name=name]').focus();
	Admin.add_to_list();
	Admin.remove_to_list();
	Admin.typeahead();
	Admin.update_movie_key();
	Admin.load_imdb_data();
	Admin.load_movie_preview();
	Admin.upload();
	Admin.remove_image();
	Admin.no_image();
	Admin.datepicker();
	Admin.revision();
	aura.path.media_files = '{$smarty.const.MEDIA_FILES_URL}';
{/block}