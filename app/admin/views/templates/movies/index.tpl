{block name="body"}
<div class="row">
  <div class="span12">
    <ul class="breadcrumb">
    	<li><a href="{$aura->root}/home">Home</a> <span class="divider">/</span></li>
      	<li>Filmes</li>
      	<li>( {$movies.pagination->registers} )</li>
    </ul>
  </div>
</div>

<div class="space4"></div>

<div class="row">
	<div class="span12 ar">
		{if $userpermission == 1}
		<a href="{$aura->root}/movies/add" class="btn btn-large btn-primary">Novo filme</a>
		{/if}
	</div>
</div>

<div class="space4"></div>

<div class="row">
	<div class="span12">
		<form action="{$aura->root}/movies" class="form-search framework-filter" method="get">
			<input type="text" name="title" class="input-medium search-query span2" value="{$movies.filter->title|default:''}" placeholder="Digite o nome do filme">
			<select name="type">
				<option {if $movies.filter->type == 'all'}selected="selected"{/if} value="all">Publicados & Não publicados</option>
				<option {if $movies.filter->type == 'published'}selected="selected"{/if} value="published">Filmes publicados</option>
				<option {if $movies.filter->type == 'unpublished'}selected="selected"{/if} value="unpublished">Filmes despublicados</option>
			</select>
			<select name="sinopsy">
				<option {if $movies.filter->sinopsy == 'all'}selected="selected"{/if} value="all">Revisadas & Não revisadas</option>
				<option {if $movies.filter->sinopsy == 'revised'}selected="selected"{/if} value="revised">Sinopses revisadas</option>
				<option {if $movies.filter->sinopsy == 'not-reviewed'}selected="selected"{/if} value="not-reviewed">Sinopses não revisadas</option>
			</select>
			<select name="period" class="span2">
				<option {if $movies.filter->period == 'all'}selected="selected"{/if} value="all">Todos os filmes</option>
				<option {if $movies.filter->period == 'release'}selected="selected"{/if} value="release">Próximas estréias</option>
			</select>
			<select name="time" class="span22">
				<option {if $movies.filter->time == 'all'}selected="selected"{/if} value="all">Duração</option>
				<option {if $movies.filter->time == 'none'}selected="selected"{/if} value="none">Sem duração</option>
			</select>
			<button type="submit" class="btn">Filtrar</button>
		</form>
	</div>
</div>

<div class="space2"></div>
<div class="container-alert-message"></div>

<div class="row">
	<div class="span12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="col40">TÍTULO</th>
					<th class="col15 ac">STATUS</th>
					<th class="col10">DURAÇÃO</th>
					<th class="col10">ESTRÉIA</th>
					<th class="col10 ac">COMMENTS</th>
					<th class="col5 ac">PUBLICADO</th>
					<th class="col10"></th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$movies.data item=movie}
				<tr class="{$movie->class_current_status}">
					<td>
						<a href="{$aura->root}/movies/edit/{$movie->id}">{$movie->name}</a>
						<small class="info">{$movie->original_name}</small>
						<div class="space1"></div>
						<small class="info min dark-light"><span class="icon-picture" title="Imagens do filme"></span> {$movie->images|default:'Imagens cadastradas!'}</small>
					</td>
					<td class="ac">{$movie->current_status}</td>
					<td>{$movie->time} min</td>
					<td><strong>{$movie->type}</strong></td>
					<td class="ac"><span class="badge badge-inverse"><a href="#" title="{$movie->comments} comentários">{$movie->comments}</a></span></td>
					<td class="ac"><span class="icon-{if $movie->status == 0}remove{elseif $movie->status == 1}ok{/if}"></span></td>
					<td class="ac">
						{if $userpermission == 1}
						<a href="#" class="btn btn-danger" rel="remove-item-table" data-item-id="{$movie->id}" data-item-name="{$movie->name}" title="Remover o filme: {$movie->name}">&nbsp;x&nbsp;</a>
						{/if}
					</td>
				</tr>
				{foreachelse}
				<tr>
					<td colspan="8">Nenhum filme cadastrado</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="span12">
		{if $movies.pagination->pages > 1}
		<div class="pagination ac">
		  <ul>
		  	{if $movies.pagination->page == 1}
		  	<li class="active">
		  		<a href="#">←</a>
		  	</li>
		  	{else}
		  	<li>
		  		<a href="{$aura->root}{$movies.pagination->url}/page/{$movies.pagination->page-1}">←</a>
		  	</li>
		  	{/if}

		  	{section name=page start=0 loop=$movies.pagination->pages step=1}
				{if $movies.pagination->page == $smarty.section.page.index+1}
					<li class="active"><a href="#">{$smarty.section.page.index+1}</a></li>
				{else}
					<li><a href="{$aura->root}{$movies.pagination->url}/page/{$smarty.section.page.index+1}">{$smarty.section.page.index+1}</a></li>
				{/if}
			{/section}

			{if $movies.pagination->page == $movies.pagination->pages}
		   	<li class="active">
		   		<a href="#">→</a>
		   	</li>
		   	{else}
		   	<li>
		   		<a href="{$aura->root}{$movies.pagination->url}/page/{$movies.pagination->page+1}">→</a>
		   	</li>
		   	{/if}
		  </ul>
		</div>
		{/if}
	</div>
</div>
{/block}

{block name="script"}
	Admin.remove_item_table();
	{if $movie_status}
	Admin.alert_messages('{$movie_status}', "{$movie_message}");
	{/if}
{/block}