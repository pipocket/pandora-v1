{block name="body"}
<div class="row">
  <div class="span12">
    <ul class="breadcrumb">
    	<li><a href="{$aura->root}/home">Home</a> <span class="divider">/</span></li>
      	<li>Fale conosco</li>
      	<li>( {$feedback.pagination->registers} )</li>
    </ul>
  </div>
</div>

<div class="space4"></div>

<div class="row">
	<div class="span9">
		<form action="{$aura->root}/feedback" class="form-search framework-filter" method="get">
			<input type="text" name="comment" class="input-medium search-query span3" value="{$feedback.filter->comment|default:''}">
			<button type="submit" class="btn">Filtrar</button>
		</form>
	</div>
</div>

<div class="space4"></div>
<div class="container-alert-message"></div>

<div class="row">
	<div class="span12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="col5">#</th>
					<th class="col60">MENSAGEM</th>
					<th class="col20 ac">USUÁRIO</th>
					<th class="col15">DATA</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$feedback.data item=message}
				<tr>
					<td><strong>{$message->id}</strong></td>
					<td>{$message->comment}</td>
					<td class="ac">{$message->user_data->name}</td>
					<td>{$message->date|date_format:'d/m/Y H:i:s'}</td>
				</tr>
				{foreachelse}
				<tr>
					<td colspan="4">Nenhuma mensagem disponível</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="span12">
		{if $feedback.pagination->pages > 1}
		<div class="pagination ac">
		  <ul>
		  	{if $feedback.pagination->page == 1}
		  	<li class="active">
		  		<a href="#">←</a>
		  	</li>
		  	{else}
		  	<li>
		  		<a href="{$aura->root}{$feedback.pagination->url}/page/{$feedback.pagination->page-1}">←</a>
		  	</li>
		  	{/if}

		  	{section name=page start=0 loop=$feedback.pagination->pages step=1}
				{if $feedback.pagination->page == $smarty.section.page.index+1}
					<li class="active"><a href="#">{$smarty.section.page.index+1}</a></li>
				{else}
					<li><a href="{$aura->root}{$feedback.pagination->url}/page/{$smarty.section.page.index+1}">{$smarty.section.page.index+1}</a></li>
				{/if}
			{/section}

			{if $feedback.pagination->page == $feedback.pagination->pages}
		   	<li class="active">
		   		<a href="#">→</a>
		   	</li>
		   	{else}
		   	<li>
		   		<a href="{$aura->root}{$feedback.pagination->url}/page/{$feedback.pagination->page+1}">→</a>
		   	</li>
		   	{/if}
		  </ul>
		</div>
		{/if}
	</div>
</div>
{/block}

{block name="script"}
	Admin.remove_item_table();
	{if false}
	Admin.alert_messages('{$movie_status}', "{$movie_message}");
	{/if}
{/block}