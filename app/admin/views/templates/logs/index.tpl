{block name="body"}
<div class="row">
  <div class="span12">
    <ul class="breadcrumb">
    	<li><a href="{$aura->root}/home">Home</a> <span class="divider">/</span></li>
		<li>
			Logs
		</li>
    </ul>
  </div>
</div>

<div class="space4"></div>

<div class="row">
	<div class="span12">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#1" data-toggle="tab">Site</a></li>
				<li><a href="#2" data-toggle="tab">Pandora</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="1">
					<table class="table table-striped">
						<colgroup>
							<col width="15%" />
							<col width="70%" />
							<col width="15%" />
						</colgroup>
						<thead>
							<tr>
								<th>Data</th>
								<th>Descrição</th>
								<th>Autor</th>
							</tr>
						</thead>
						<tbody>
							{foreach from=$commits->site item=item}
							<tr>
								<td>
									{if $item.commit.author.date|date_format:'Y-m-d' == $smarty.now|date_format:'Y-m-d'}
										Hoje às {$item.commit.author.date|date_format:'H:i'}
									{else}
										{$item.commit.author.date|date_format:'d/m/Y H:i'}{/if}
								</td>
								<td>{$item.commit.message}</td>
								<td>{$item.commit.author.name}</td>
							</tr>
							{/foreach}
						</tbody>
					</table>
				</div>

				<div class="tab-pane" id="2">
					<table class="table table-striped">
						<colgroup>
							<col width="15%" />
							<col width="70%" />
							<col width="15%" />
						</colgroup>
						<thead>
							<tr>
								<th>Data</th>
								<th>Descrição</th>
								<th>Autor</th>
							</tr>
						</thead>
						<tbody>
							{foreach from=$commits->admin item=item}
							<tr>
								<td>
									{if $item.commit.author.date|date_format:'Y-m-d' == $smarty.now|date_format:'Y-m-d'}
										Hoje às {$item.commit.author.date|date_format:'H:i'}
									{else}
										{$item.commit.author.date|date_format:'d/m/Y H:i'}{/if}
								</td>
								<td>{$item.commit.message}</td>
								<td>{$item.commit.author.name}</td>
							</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
{/block}

{block name="script"}
{/block}