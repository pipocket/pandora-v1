<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{$smarty.const.PROJECT_NAME} - Administração</title>
    <meta name="description" content="">
    <meta name="author" content="">
 
    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="{$html->css}/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{$html->css}/login.css">
</head>
<body>
    <div class="container">
        <div class="content">
            <div class="row">
                {if $error_login}
                <div class="alert alert-error">
                  Dados inválidos. Tente novamente.
                </div>
                {/if}

                <div class="login-form">
                    <form action="{$aura->root}/login/in" method="post">
                        <fieldset>
                            <div class="clearfix">
                                <input type="text" placeholder="Email" class="input" name="email" value="{$email_login}" />
                            </div>
                            <div class="clearfix">
                                <input type="password" placeholder="Password" class="input" name="password" />
                            </div>

                            <input type="hidden" name="url_target" value="{$url_target}" />
                            <button class="btn btn-primary fr" type="submit">Sign in</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div> <!-- /container -->
</body>
</html>