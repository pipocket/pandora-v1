<?php
    class AppModel extends aura_model
	{
		public function date_br_format( $date )
		{
			if ( $date )
				return join( '/', array_reverse( explode( '-', reset( explode( ' ', $date ) ) ) ) );
			return NULL;
		}

		public function date_sql_format( $date )
		{
			if ( $date == '__/__/____' )
				$date = null;

			if ( $date )
				return join( '-', array_reverse( explode( '/', $date ) ) );
			
			return NULL;
		}

		public function _request( $name, $if_not_exists = NULL, $type = 'post', $if_on = 1 )
		{
			if ( isset( $_POST ) ) {
				if ( isset( $_POST[$name] ) ) {
					if ( $_POST[$name] ) {
						if ( $_POST[$name] == 'on' )
							return $if_on;
						else {
							return $_POST[$name];
						}
					}
					else
						return $if_not_exists;
				}
				else
					return $if_not_exists;
			}
		}

		public function last( $field_select = 'name', $field_order = 'date' )
		{
			$sql = $this->database()->prepare( "SELECT id, {$field_select} FROM {$this->table_name} ORDER BY `{$field_order}` DESC LIMIT 10" );
			$sql->execute();

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function mail( $mail_from, $body, $subject )
		{
			$mail = $this->vendors( 'phpmailer' );
			$mail->IsSMTP();

			$mail->Host = AURA_SMTP_HOST;
			$mail->SMTPAuth = AURA_SMTP_AUTH;
			$mail->SMTPSecure = "ssl";
			$mail->SMTPKeepAlive = true;
			$mail->Host = AURA_SMTP_HOST;
			$mail->Port = AURA_SMTP_PORT;
			$mail->Username = AURA_SMTP_USERNAME;
			$mail->Password = AURA_SMTP_PASSWORD;

			$mail->SetFrom( $mail_from, AURA_SMTP_FROMNAME );
			$mail->Subject = $subject;
			$mail->MsgHTML( $body );
			
			return $mail;
		}
	}
?>