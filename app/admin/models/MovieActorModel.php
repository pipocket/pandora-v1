<?php
    class MovieActorModel extends AppModel
	{
		protected $table_name = "movie_actor";
		protected $table_key = "";
		
		public function save_list( $movie_id )
		{
			$this->remove( $movie_id );
			
			$actors = $this->_request( 'actors' );
			$result = false;

			if ( is_array( $actors ) ) {
				foreach ( $actors as $actor ) {
					if ( !is_numeric($actor) )
						$actor = $this->Actor->add( $actor );

					$data = array(
						'data' => array (
								'actor_id' => $actor,
								'movie_id' => $movie_id
							)
					);

					$result = $this->set( $data );
				}
			}

			return $result;
		}

		public function remove( $movie_id )
		{
			$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE movie_id = ?" );
			return $sql->execute( array( $movie_id ) );
		}

		public function by_movie( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT ma.*, a.name FROM {$this->table_name} ma JOIN actor a ON a.id = ma.actor_id WHERE ma.movie_id = ? ORDER BY a.name" );
			$sql->execute( array( $movie_id ) );
			$actors = $sql->fetchAll( PDO::FETCH_OBJ );

			return $actors;
		}
	}
?>