<?php
    class UserModel extends AppModel
	{
		protected $table_name = "user";
		protected $table_key = "id";
		
		public function login()
		{
			if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
				$url_target = $_POST['url_target'];
				$email = isset( $_POST['email'] ) ? addslashes( $_POST['email'] ) : null;
				$password = isset( $_POST['password'] ) ? Helper::md5( addslashes( $_POST['password'] ) ) : null;

				if ( $email && $password ) {
					$sql = $this->database()->prepare( "SELECT u.id, u.name, u.email, u.security, au.id as admin_id, au.admin_permission_id FROM {$this->table_name} u JOIN admin_user au ON au.user_id = u.id WHERE u.email = ? AND u.password = ? AND u.status = 1" );
					$sql->execute( array( $email, $password ) );
					$user = $sql->fetch( PDO::FETCH_OBJ );
				}

				if ( isset( $user ) AND $user ) {
					$this->login_session( $user );
					$this->Log->register( 1 );

					$this->redirect( $url_target ? $url_target : '/' );
				}
				else {
					$this->message( 'error_login', true );
					$this->message( 'email_login', $email );
					$this->message( 'url_target', $url_target );

					$this->redirect( '/login' );	
				}

				exit;
			}

			$this->redirect( '/login' );
		}

		// void
		private function login_session( $user )
		{
			$usertime = explode( ' ', date( 'Y-m-d H:i:s' ) );
			$usertime[0] = explode( '-', $usertime[0] );
			$usertime[1] = explode( ':', $usertime[1] );
			$usertime = date( 'Y-m-d H:i:s', mktime( $usertime[1][0]+3,$usertime[1][1],$usertime[1][2],$usertime[0][1],$usertime[0][2],$usertime[0][0] ) );

			$this->session( 'userid', base64_encode( $user->id ) );
			$this->session( 'username', base64_encode( $user->name ) );
			$this->session( 'useremail', base64_encode( $user->email ) );
			$this->session( 'usertime', $usertime );

			$this->session( 'user_admin_id', base64_encode( $user->admin_id ) );
			$this->session( 'user_admin_permission', base64_encode( $user->admin_permission_id ) );
		}

		public function get_admin_user_id()
		{
			return base64_decode( $this->session( 'user_admin_id' ) );
		}

		public function get_admins( $permission )
		{
			if ( $permission == 0 )
				$sql = $this->database()->prepare( "SELECT u.name, u.email FROM admin_user au JOIN {$this->table_name} u ON u.id = au.user_id" );
			else
				$sql = $this->database()->prepare( "SELECT u.name, u.email FROM admin_user au JOIN {$this->table_name} u ON u.id = au.user_id WHERE au.admin_permission_id = ?" );

			$sql->execute( array( $permission ) );

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}
	}
?>