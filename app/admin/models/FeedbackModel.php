<?php
    class FeedbackModel extends AppModel
	{
		protected $table_name = "feedback";
		protected $table_key = "id";
		
		public function all( $pagination )
		{
			$comment = isset( $_GET['comment'] ) ? $_GET['comment'] : null;
			$type = isset( $_GET['type'] ) ? $_GET['type'] : null;
			$sinopsy = isset( $_GET['sinopsy'] ) ? $_GET['sinopsy'] : null;
			$additional_where = '';

			if ( $comment )
				$additional_where = " AND comment LIKE '%{$comment}%'";

			if ( $type && $type != 'all' ) {
				$status = $type == 'published' ? 1 : 0;
				$additional_where .= " AND status = $status";
			}
			if ( $sinopsy && $sinopsy != 'all' ) {
				$storyline_check = $sinopsy == 'revised' ? 1 : 0;
				$additional_where .= " AND storyline_check = $storyline_check";
			}

			$feedback = $this->getAll( array('where' => 'id > 0' . $additional_where, 'order' => 'date desc', 'pagination' => $pagination ) );

			$feedback['filter']->comment = $comment;
			$feedback['filter']->type = $type;
			$feedback['filter']->sinopsy = $sinopsy;

			return $feedback;
		}
	}
?>