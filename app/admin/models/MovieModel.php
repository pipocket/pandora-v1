<?php
    class MovieModel extends AppModel
	{
		protected $table_name = "movie";
		protected $table_key = "id";
		protected $table_exception = "status <> 2";
		
		public function all( $pagination )
		{
			$title = isset( $_GET['title'] ) ? $_GET['title'] : null;
			$type = isset( $_GET['type'] ) ? $_GET['type'] : null;
			$sinopsy = isset( $_GET['sinopsy'] ) ? $_GET['sinopsy'] : null;
			$period = isset( $_GET['period'] ) ? $_GET['period'] : null;
			$time = isset( $_GET['time'] ) ? $_GET['time'] : null;
			$additional_where = '';
			$order = 'name asc';

			if ( $title )
				$additional_where = " AND name LIKE '%{$title}%'";

			if ( $type && $type != 'all' ) {
				$status = $type == 'published' ? 1 : 0;
				$additional_where .= " AND status = $status";
				$order = '`release` asc';
			}
			if ( $sinopsy && $sinopsy != 'all' ) {
				$storyline_check = $sinopsy == 'revised' ? 1 : 0;
				$additional_where .= " AND storyline_check = $storyline_check";
			}
			if ( $period && $period != 'all' ) {
				$additional_where .= " AND date_format(`release`, '%Y%m%d') >= curdate()";
			}
			if ( $time && $time != 'all' ) {
				$additional_where .= " AND time = 0";
			}

			$movies = $this->getAll( array('where' => 'id > 0' . $additional_where, 'order' => $order, 'pagination' => $pagination) );

			foreach ( $movies['data'] as $movie ) {
				$movie->type = ( date('Y-m-d', strtotime( $movie->release ) ) >= date( 'Y-m-d' ) ) ? 'Estréia ' .  date('d/m', strtotime( $movie->release )) : 'Em cartaz';
				$movie->comments = $this->Rate->count_comment( $movie->id );

				// Current status
				if ( $movie->status == 0 AND $movie->revision == 0 ) {
					$movie->class_current_status = 'unpublished';
					$movie->current_status = 'Em processo';
				}
				else if ( $movie->status == 1 ) {
					$movie->class_current_status = '';
					$movie->current_status = 'Ok';
				}
				else if ( $movie->revision == 1 ) {
					$movie->class_current_status = 'check';
					$movie->current_status = 'Revisar';
				}
				else if ( $movie->revision == 2 ) {
					$movie->class_current_status = 'publish';
					$movie->current_status = 'Publicar';
				}

				// Images
				$images = null;
				$thumbs = $this->MovieThumb->by_movie( $movie->id );

				// Poster
				$banners = $this->MoviePoster->by_movie( $movie->id );
				$images = !( $banners AND count( $banners ) > 0 ) ? 'Falta Banner' : '';

				// Others
				if ( $thumbs ) {
					$images .= !$thumbs->thumb1 ? $images ? ', Falta Ranking' : 'Falta Ranking' : '';
					$images .= !$thumbs->thumb2 ? $images ? ', Falta Thumb' : 'Falta Thumb' : '';
					$images .= !$thumbs->facebook ? $images ? ', Falta Facebook' : 'Falta Facebook' : '';
					$images .= !$thumbs->poster ? $images ? ', Falta Poster' : 'Falta Poster' : '';
				}
				else {
					$images .= $images ? ', Falta Ranking' : 'Falta Ranking';
					$images .= ', Falta Thumb';
					$images .= ', Falta Facebook';
					$images .= ', Falta Poster';
				}

				$movie->images = $images;
			}

			$movies['filter']->title = $title;
			$movies['filter']->type = $type;
			$movies['filter']->sinopsy = $sinopsy;
			$movies['filter']->period = $period;
			$movies['filter']->time = $time;

			return $movies;
		}

		public function one( $id )
		{
			$movie = $this->getById( $id );
			$movie['data']->preview = $this->date_br_format( $movie['data']->preview );
			$movie['data']->release = $this->date_br_format( $movie['data']->release );
			$movie['data']->genders = $this->MovieGender->by_movie( $movie['data']->id );
			$movie['data']->actors = $this->MovieActor->by_movie( $movie['data']->id );
			$movie['data']->directors = $this->MovieDirector->by_movie( $movie['data']->id );
			$movie['data']->poster = $this->MoviePoster->by_movie( $movie['data']->id );
			$movie['data']->thumbs = $this->MovieThumb->by_movie( $movie['data']->id );

			return $movie['data'];
		}

		public function save()
		{
			$data = array(
					'data' => array (
							'id' => $this->_request( 'movie_id' ),
							'name' => $this->_request( 'name' ),
							'original_name' => $this->_request( 'original_name' ),
							'age' => $this->_request( 'age' ),
							'time' => $this->_request( 'time', 0 ),
							'storyline' => str_replace( "\\\\", "\\", str_replace( '"', '\"', $this->_request( 'storyline' ) ) ),
							'storyline_check' => $this->_request( 'storyline_check', 0 ),
							'revision' => $this->_request( 'revision', 0 ),
							'year' => $this->_request( 'year' ),
							'origin' => $this->_request( 'origin' ),
							'key' => $this->_request( 'key' ),
							'preview' => $this->date_sql_format( $this->_request( 'preview' ) ),
							'release' => $this->date_sql_format( $this->_request( 'release' ) ),
							'status' => $this->_request( 'publish', 0 )
						)
				);

			// Revisino rule
			$data['data']['status'] = $data['data']['revision'] < 2 ? 0 : $data['data']['status'];

			// Alert mail
			if ( $this->_request( 'revision_old' ) != $data['data']['revision'] ) {
				switch ( $data['data']['revision'] ) {
					case 0:
						$recipients = $this->User->get_admins(0);
						$status_now = 'Despublicado';
						break;
					
					case 1:
						$recipients = $this->User->get_admins(2);
						$status_now = 'Já pode ser revisado';
						break;

					case 2:
						$recipients = $this->User->get_admins(1);
						$status_now = 'Já pode ser publicado';
						break;
				}

				$body_mail = $this->controller('home')->template_get_content( 'movie-revision', '_Mail' );

				$body_mail = str_replace( '#MOVIE_NAME', $data['data']['name'], $body_mail );
				$body_mail = str_replace( '#MOVIE_ID', $data['data']['id'], $body_mail );
				$body_mail = str_replace( '#STATUS', $status_now, $body_mail );
				$body_mail = str_replace( '#SITE_URL', SITE_INFO_DOMAIN, $body_mail );
				$body_mail = str_replace( '#ADMIN_URL', $_SERVER['HTTP_HOST'], $body_mail );

				$send_mail = $this->mail( ACTIVATION_MAIL, $body_mail, utf8_decode( '\'' . $data['data']['name'] . '\' ' . $status_now ) );

				foreach ( $recipients as $recipient ) {
					$send_mail->AddAddress( $recipient->email );
				}

				$send_mail->send();
			}
			
			$response = $this->set( $data );

			if ( $response ) {
				$movie_id = is_numeric( $response ) ? $response : $this->_request( 'movie_id' );
				$update = is_numeric( $response ) ? false : true;
				$thumb1 = null;
				$thumb2 = null;
				$poster = null;
				$facebook = null;
				$thumb_facebook = null;

				$this->MovieGender->save_list( $movie_id );
				$this->MovieActor->save_list( $movie_id );
				$this->MovieDirector->save_list( $movie_id );

				$upload = $this->vendors( 'upload' );

				// Clear posters
				$this->MoviePoster->remove( $movie_id );

				if ( $this->_request( 'banner' ) ) {
					foreach ( $this->_request( 'banner' ) as $key => $banner ) {
						$key += 1;

						if ( substr( $banner, 0, 4 ) != 'old#' ) {
							$new_file_name = "{$movie_id}_{$key}_934x184";
							$status = $upload->rename( $banner, $new_file_name, '/movies' );

							// For save
							$new_file_name .= Helper::get_image_extension( $banner );
						}
						else
							$new_file_name = substr( $banner, 4, strlen( $banner ) );
						
						$this->MoviePoster->save( $movie_id, $new_file_name );
					}
				}

				if ( $this->_request( 'thumb1' ) ) {
					if ( substr( $this->_request( 'thumb1' ), 0, 4 ) != 'old#' ) {
						$new_file_name = "{$movie_id}_270x97";
						$upload->rename( $this->_request( 'thumb1' ), $new_file_name, '/movies' );

						// For save
						$thumb1 = $new_file_name . Helper::get_image_extension( $this->_request( 'thumb1' ) );
					}
					else
						$thumb1 = substr( $this->_request( 'thumb1' ), 4, strlen( $this->_request( 'thumb1' ) ) );
				}

				if ( $this->_request( 'thumb2' ) ) {
					if ( substr( $this->_request( 'thumb2' ), 0, 4 ) != 'old#' ) {
						$new_file_name = "{$movie_id}_108x108";
						$upload->rename( $this->_request( 'thumb2' ), $new_file_name, '/movies' );

						// For save
						$thumb2 = $new_file_name . Helper::get_image_extension( $this->_request( 'thumb2' ) );
					}
					else
						$thumb2 = substr( $this->_request( 'thumb2' ), 4, strlen( $this->_request( 'thumb2' ) ) );
				}

				if ( $this->_request( 'poster' ) ) {
					if ( substr( $this->_request( 'poster' ), 0, 4 ) != 'old#' ) {
						$new_file_name = "{$movie_id}_poster";
						$upload->rename( $this->_request( 'poster' ), $new_file_name, '/movies' );

						// For save
						$poster = $new_file_name . Helper::get_image_extension( $this->_request( 'poster' ) );
					}
					else
						$poster = substr( $this->_request( 'poster' ), 4, strlen( $this->_request( 'poster' ) ) );
				}

				if ( $this->_request( 'facebook' ) ) {
					if ( substr( $this->_request( 'facebook' ), 0, 4 ) != 'old#' ) {
						$new_file_name = "{$movie_id}_90x90";
						$upload->rename( $this->_request( 'facebook' ), $new_file_name, '/movies' );

						// For save
						$facebook = $new_file_name . Helper::get_image_extension( $this->_request( 'facebook' ) );
					}
					else
						$facebook = substr( $this->_request( 'facebook' ), 4, strlen( $this->_request( 'facebook' ) ) );
				}
				
				// Clear thumbs
				$this->MovieThumb->remove( $movie_id );

				if ( $thumb1 OR $thumb2 OR $poster OR $facebook )
					$this->MovieThumb->save( $movie_id, $thumb1, $thumb2, $poster, $facebook );
			}

			$result = array();
			$result['status'] = isset( $response['error'] ) ? false : true;
			$result['update'] = $update;
			$result['movie_name'] = $this->_request( 'name' );
			$result['movie_id'] = $movie_id;

			if ( isset( $response['error'] ) )
				Helper::log( 'error', $response['error'] );

			return (object)$result;
		}

		public function key( $key, $movie_id )
		{
			if ( $movie_id )
				$result = $this->getByKey( $key, array('where' => "id <> $movie_id") );
			else
				$result = $this->getByKey( $key );

			$result = count($result['data']);

			return $result == 0 ? true : false;
		}

		public function remove()
		{
			$id = isset( $_POST['id'] ) ? $_POST['id'] : null;
			$result = false;

			if ( $id ) {
				$sql = $this->database()->prepare( "UPDATE {$this->table_name} SET status = 2 WHERE id = ?" );
				$result = $sql->execute( array( $id ) );
			}

			return $result;
		}
	}
?>