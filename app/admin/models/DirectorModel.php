<?php
    class DirectorModel extends AppModel
	{
		protected $table_name = "director";
		protected $table_key = "id";
		
		public function all()
		{
			$movies = $this->getAll( array('order' => 'name asc') );
			return $movies;
		}

		public function add( $name )
		{
			$name = trim( $name );
			$director = $this->getByName( $name );
			
			if ( count( $director['data'] ) > 0 )
				return $director['data'][0]->id;
			
			$data = array(
				'data' => array (
						'name' => $name
					)
			);

			return $this->set( $data );
		}
	}
?>