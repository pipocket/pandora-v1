<?php
    class ActorModel extends AppModel
	{
		protected $table_name = "actor";
		protected $table_key = "id";
		
		public function all()
		{
			$movies = $this->getAll( array('order' => 'name asc') );
			return $movies;
		}

		public function add( $name )
		{
			$name = trim( $name );
			$actor = $this->getByName( $name );
			
			if ( count( $actor['data'] ) > 0 )
				return $actor['data'][0]->id;
			
			$data = array(
				'data' => array (
						'name' => $name
					)
			);

			return $this->set( $data );
		}
	}
?>