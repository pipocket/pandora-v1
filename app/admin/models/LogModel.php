<?php
	class LogModel extends AppModel
	{
		protected $table_name = "admin_log";
		protected $table_key = "id";

		public function register( $log_type, $description = null )
		{
			$sl = $this->database()->prepare( "INSERT INTO {$this->table_name} (description, admin_user_id, admin_log_type_id) VALUES (?,?,?)" );
			$sl->execute( array( $description, $this->User->get_admin_user_id(), $log_type ) );
		}
	}
?>