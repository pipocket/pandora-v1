<?php
    class MovieGenderModel extends AppModel
	{
		protected $table_name = "movie_gender";
		protected $table_key = "";
		
		public function save_list( $movie_id )
		{
			$this->remove( $movie_id );

			$genders = $this->_request( 'genders' );
			$result = false;

			if ( is_array( $genders ) ) {
				foreach ( $genders as $gender ) {
					if ( !is_numeric($gender) )
						$gender = $this->Gender->add( $gender );

					$data = array(
						'data' => array (
								'gender_id' => $gender,
								'movie_id' => $movie_id
							)
					);

					$result = $this->set( $data );
				}
			}

			return $result;
		}

		public function remove( $movie_id )
		{
			$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE movie_id = ?" );
			return $sql->execute( array( $movie_id ) );
		}

		public function by_movie( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT mg.*, g.description FROM {$this->table_name} mg JOIN gender g ON g.id = mg.gender_id WHERE mg.movie_id = ? ORDER BY g.description" );
			$sql->execute( array( $movie_id ) );
			$genders = $sql->fetchAll( PDO::FETCH_OBJ );

			return $genders;
		}
	}
?>