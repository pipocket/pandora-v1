<?php
    class RateModel extends AppModel
	{
		protected $table_name = "rate";
		protected $table_key = "id, user_id, movie_id";
		
		public function count_comment( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT COUNT(*) as n from rate r JOIN `comment` c ON c.rate_id = r.id WHERE r.movie_id = ?" );
			$sql->execute( array( $movie_id ) );

			return $sql->fetch( PDO::FETCH_OBJ )->n;
		}
	}
?>