<?php
    class MovieThumbModel extends AppModel
	{
		protected $table_name = "movie_thumb";
		protected $table_key = "";
		
		public function save( $movie_id, $thumb1, $thumb2, $poster, $facebook )
		{
			$data = array(
				'data' => array (
						'movie_id' => $movie_id,
						'thumb1' => $thumb1,
						'thumb2' => $thumb2,
						'poster' => $poster,
						'facebook' => $facebook
					)
			);

			return $this->set( $data );
		}

		public function by_movie( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT * FROM {$this->table_name} WHERE movie_id = ?" );
			$sql->execute( array( $movie_id ) );

			return $sql->fetch( PDO::FETCH_OBJ );
		}

		public function remove( $movie_id )
		{
			$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE movie_id = ?" );
			return $sql->execute( array( $movie_id ) );
		}
	}
?>