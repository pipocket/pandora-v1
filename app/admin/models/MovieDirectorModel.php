<?php
    class MovieDirectorModel extends AppModel
	{
		protected $table_name = "movie_director";
		protected $table_key = "";
		
		public function save_list( $movie_id )
		{
			$this->remove( $movie_id );
			
			$directors = $this->_request( 'directors' );
			$result = false;

			if ( is_array( $directors ) ) {
				foreach ( $directors as $director ) {
					if ( !is_numeric($director) )
						$director = $this->Director->add( $director );

					$data = array(
						'data' => array (
								'director_id' => $director,
								'movie_id' => $movie_id
							)
					);

					$result = $this->set( $data );
				}
			}

			return $result;
		}

		public function remove( $movie_id )
		{
			$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE movie_id = ?" );
			return $sql->execute( array( $movie_id ) );
		}

		public function by_movie( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT md.*, d.name FROM {$this->table_name} md JOIN director d ON d.id = md.director_id WHERE md.movie_id = ? ORDER BY d.name" );
			$sql->execute( array( $movie_id ) );
			$directors = $sql->fetchAll( PDO::FETCH_OBJ );

			return $directors;
		}
	}
?>