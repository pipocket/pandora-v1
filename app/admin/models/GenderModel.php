<?php
    class GenderModel extends AppModel
	{
		protected $table_name = "gender";
		protected $table_key = "id";
		
		public function all()
		{
			$movies = $this->getAll( array('order' => 'description asc') );
			return $movies;
		}

		public function add( $description )
		{
			$description = trim( $description );
			$gender = $this->getByDescription( $description );
			
			if ( count( $gender['data'] ) > 0 )
				return $gender['data'][0]->id;

			$data = array(
				'data' => array (
						'description' => $description
					)
			);

			return $this->set( $data );
		}
	}
?>