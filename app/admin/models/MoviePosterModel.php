<?php
    class MoviePosterModel extends AppModel
	{
		protected $table_name = "movie_poster";
		protected $table_key = "";
		
		public function save( $movie_id, $image )
		{
			$data = array(
				'data' => array (
						'movie_id' => $movie_id,
						'image' => $image
					)
			);

			return $this->set( $data );
		}

		public function by_movie( $movie_id )
		{
			$sql = $this->database()->prepare( "SELECT * FROM {$this->table_name} WHERE movie_id = ?" );
			$sql->execute( array( $movie_id ) );

			return $sql->fetchAll( PDO::FETCH_OBJ );
		}

		public function remove( $movie_id )
		{
			$sql = $this->database()->prepare( "DELETE FROM {$this->table_name} WHERE movie_id = ?" );
			return $sql->execute( array( $movie_id ) );
		}
	}
?>