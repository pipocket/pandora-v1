$(function(){
	Admin._init();
});

var Admin = {

	list_genders: '',
	list_actors: '',
	list_directors: '',
	
	_init: function() {
		try {

		} catch (e) {
			alert('Error: ' + e.description);
		}
	},

	clear_string: function(objResp) {  
		var varString = new String($.trim(objResp));  
		var stringSpecialChars = new String('àâêôûãõáéíóúçüÀÂÊÔÛÃÕÁÉÍÓÚÇÜ \':;.,?!`©®');
		var stringClear = new String('aaeouaoaeioucuAAEOUAOAEIOUCU_-');  

		var i = new Number();  
		var j = new Number();  
		var cString = new String();  
		var varRes = '';  

		for (i = 0; i < varString.length; i++) {  
			cString = varString.substring(i, i + 1);  

			for (j = 0; j < stringSpecialChars.length; j++) {  
				if (stringSpecialChars.substring(j, j + 1) == cString) {  
					cString = stringClear.substring(j, j + 1);  
				}  
			}  
			varRes += cString;  
		}

		if ( varRes.indexOf('__') !== -1 )
			varRes = varRes.replace("__", "_");

		if ( varRes.indexOf('__') !== -1 )
			varRes = varRes.replace("__", "_");

		return varRes.toLowerCase();  
	},

	alert_messages: function( type, message ) {
		if ( type && message ) {
			var alert = "<div class='alert alert-" + type + "'>" + message + "</div>"
			$('div.container-alert-message').html(alert);
			$('div.container-alert-message').slideDown('fast');

			setTimeout("$('div.container-alert-message').slideUp('fast')", 10000);
		}	
	},

	remove_item_table: function() {
		$("a[rel='remove-item-table']").click(function(e){
			e.preventDefault();
			
			if ( confirm('Realmente deseja remover o filme ' + $(this).attr('data-item-name') + ' ?') ) {
				var $this = $(this);
				var id = $this.attr('data-item-id');

				$this.css('background', 'url(' + aura.web.img + "/ico/ajax-loader-white.gif) no-repeat center center");

				$.post(
					aura.path.approot + '/movies/remove',
					'id=' + id,
					function( response ) {
						$this.parent().parent().fadeOut('slow');
					}, 'json'
				)
			}
		});
	},

	typeahead: function() {

		$.get( aura.path.approot + '/genders/all/method/json', function( response ) {
			Admin.list_genders = response;

			var items = new Array();

			$.each( response, function( i, val ) {
				items.push( val.description + '#' + val.id );
			});

			$('.typeahead-gender').typeahead({
				items: 4,
				source: items
			});
		}, 'json' );

		$.get( aura.path.approot + '/actors/all/method/json', function( response ) {
			Admin.list_actors = response;

			var items = new Array();

			$.each( response, function( i, val ) {
				items.push( val.name + '#' + val.id );
			});

			$('.typeahead-actor').typeahead({
				items: 4,
				source: items
			});
		}, 'json' );

		$.get( aura.path.approot + '/directors/all/method/json', function( response ) {
			Admin.list_directors = response;

			var items = new Array();

			$.each( response, function( i, val ) {
				items.push( val.name + '#' + val.id );
			});

			$('.typeahead-director').typeahead({
				items: 4,
				source: items
			});
		}, 'json' );

	},

	add_to_list: function() {
		
		$("input[data-action='add-to-list']").click(function(){
			var type = $(this).attr('data-type');
			var list = 'list_' + type;
			var item_name = $("input[type=text][data-type='" + type + "']").val();
			var item_id = $("input[type=text][data-type='" + type + "']").attr('data-id');
			var field_name = $("input[type=text][data-type='" + type + "']").attr('data-name');

			item_id = item_id ? item_id : item_name;	

			if ( !item_id ) {
				alert( "Digite algo!" );
				return false;
			}

			if ( item_id.split(',').length > 1 ) {
				items = item_id.split(',');

				for ( i=0; i<=items.length-1; i++) {
					if ( $('ul#' + list + " li input[name='" + field_name + "'][value='" + items[i] + "']").length > 0 )
							alert("Este item já esta na lista!");
					
					$('ul#' + list).append("<li><input type='hidden' name='" + field_name + "' value='" + items[i] + "'>" + items[i] + " <a href='#' class='close' rel='remove'>&times;</a></li>");
				}
			}
			else {
				// Clear field
				$("input[type=text][data-type='" + type + "']").removeAttr('data-id');
				$("input[type=text][data-type='" + type + "']").val('');	

				$('ul#' + list).append("<li><input type='hidden' name='" + field_name + "' value='" + item_id + "'>" + item_name + " <a href='#' class='close' rel='remove'>&times;</a></li>");
			}

			// Call remove function
			Admin.remove_to_list();
		});

	},

	remove_to_list: function() {

		$('a[rel=remove]').unbind('click');
		$('a[rel=remove]').bind('click', function(e){
			e.preventDefault();

			$(this).parent().slideUp('fast', function(){
				$(this).remove();
			})
		});

	},

	update_movie_key: function() {

		$("input[name='name']").blur(function(){
			var $this = $("input[name='name']");
			var name = $this.val();
			var movie_id = $('input[name=movie_id]').val();
			
			if (name.length > 0) {
				var key = Admin.clear_string( name );

				$.get( aura.path.approot + '/movies/key/' + key + '/' + movie_id, function(response) {
					if (!response) {
						$('input[name=key]').addClass('error');
						$("<span class='help-inline error'>Esta chave de url já existe.</span>").insertAfter($('input[name=key]'));
					}
					else {
						$('input[name=key]').removeClass('error');
						$('input[name=key]').next('span.help-inline.error').remove();
					}

					$('input[name=key]').val( key );
				}, 'json' );
			}
			else {
				$('input[name=key]').val( '' );
			}
		});

	},

	load_imdb_data: function() {

		$('input#load-imdb-data').click(function(){
			var $this = $(this);
			var name = $('input[name=name]').val();
			var field = $(this).prev();
			$this.attr('disabled', 'disabled');

			if (name) {
				field.css('background', 'url(' + aura.web.img + "/ico/ajax-loader-white.gif) no-repeat right center");

				$.getJSON(
					'http://www.imdbapi.com',
					't=' + name + '&y=2012&callback=?',
					function(data) {
						$this.removeAttr('disabled');
						field.css('background', 'none');

						$('div#imdb div.modal-body div.clearfix div.poster').remove();
						$('div#imdb div.modal-body div.clearfix div.movie-data').remove();
						
						$('div#imdb div.modal-header h3').html(data.Title + ' (' + data.Year + ')');

						if (data.Poster != 'N/A') {
							var image = $('<img/>');
							image.hide();
							image.attr('src', data.Poster);
							image.attr('width', 220);
							image.attr('height', 368);
							
							$('div#imdb div.modal-body div.clearfix').html("<div class='fl poster'></div>");
							$('div#imdb div.modal-body div.clearfix div.fl.poster').append(image);

							image.load(function(){
								$(this).fadeIn('fast');
							});
						}

						$('div#imdb div.modal-body div.clearfix').append("<div class='fl movie-data'></div>");

						$('div#imdb div.modal-body div.clearfix div.movie-data').append("<p>Atores: " + data.Actors + "</p>");
						$('div#imdb div.modal-body div.clearfix div.movie-data').append("<p>Diretor: " + data.Director + "</p>");
						$('div#imdb div.modal-body div.clearfix div.movie-data').append("<p>Gênero: " + data.Genre + "</p>");
						$('div#imdb div.modal-body div.clearfix div.movie-data').append("<p>Sinopse: " + data.Plot + "</p>");
						$('div#imdb div.modal-body div.clearfix div.movie-data').append("<p>Tempo: " + data.Runtime + "</p>");
					
						$('#imdb').modal({
							show: true,
							backdrop: false
						});

						$("a[rel='load-data']").unbind('click');
						$("a[rel='load-data']").bind('click', function(e){
							e.preventDefault();
							var hour = 0;
							var min = 0;
							var actors = data.Actors.split(',');
							var directors = data.Director.split(',');
							var genders = data.Genre.split(',');

							if (data.Runtime != 'N/A') {
								time = data.Runtime.split(' hr');
								hour = time.length > 1 ? parseInt(time[0]) * 60 : 0;
								min = time[time.length-1].split(' mins')[0]; 
								min = min.split('s ').length > 1 ? parseInt(min.split('s ')[1]) : parseInt(min);
							}

							// Verify Genders
							for(i=0; i<genders.length; i++) {
								$.each(Admin.list_genders, function(j, val){
									if ($.trim(genders[i]) == val.description_en) {

										if($("ul#list_genders li input[name='genders[]'][value='" + val.id + "']").length == 0)
										$('ul#list_genders').append("<li><input type='hidden' name='genders[]' value='" + val.id + "'>" + val.description + "<a href='#' class='close' rel='remove'>&times;</a></li>");
										genders[i] = null;
									}
								});
							}
							
							for(i=0; i<genders.length; i++) {
								if (genders[i])
									$('ul#list_genders').append("<li><input type='hidden' name='genders[]' value='" + $.trim(genders[i]) + "'>" + $.trim(genders[i]) + "<a href='#' class='close' rel='remove'>&times;</a></li>");
							}

							// Verify Actors
							for(i=0; i<actors.length; i++) {
								$.each(Admin.list_actors, function(j, val){
									if ($.trim(actors[i]) == val.name) {

										if($("ul#list_actors li input[name='actors[]'][value='" + val.id + "']").length == 0)
										$('ul#list_actors').append("<li><input type='hidden' name='actors[]' value='" + val.id + "'>" + val.name + "<a href='#' class='close' rel='remove'>&times;</a></li>");
										actors[i] = null;
									}
								});
							}
							
							for(i=0; i<actors.length; i++) {
								if (actors[i])
									$('ul#list_actors').append("<li><input type='hidden' name='actors[]' value='" + $.trim(actors[i]) + "'>" + $.trim(actors[i]) + "<a href='#' class='close' rel='remove'>&times;</a></li>");
							}

							// Verify Directors
							for(i=0; i<directors.length; i++) {
								$.each(Admin.list_directors, function(j, val){
									if ($.trim(directors[i]) == val.name) {
										$('ul#list_directors').append("<li><input type='hidden' name='directors[]' value='" + val.id + "'>" + val.name + "<a href='#' class='close' rel='remove'>&times;</a></li>");
										directors[i] = null;
									}
								});
							}
							
							for(i=0; i<directors.length; i++) {
								if (directors[i])
									$('ul#list_directors').append("<li><input type='hidden' name='directors[]' value='" + $.trim(directors[i]) + "'>" + $.trim(directors[i]) + "<a href='#' class='close' rel='remove'>&times;</a></li>");
							}

							$("input[name='original_name']").val(data.Title).blur();
							$("input[name='original_name']").val(data.Title);
							$("input[name='time']").val(hour+min);

							$('#imdb').modal('hide');

							Admin.remove_to_list();
						});
					}
				)
			}
		});

	},

	load_movie_preview: function() {
		$('input#load-movie-previews').click(function(){
			var field = $(this).prev();
			var current_date = null;

			if ( $('div#moviepreview div.modal-body div.modal-moviepreview-info ul li').length > 0 ) {
				Admin.modal_movie_preview();
				return false;
			}

			field.css('background', 'url(' + aura.web.img + "/ico/ajax-loader-white.gif) no-repeat right center");
			
			$.getJSON
			(
				aura.path.approot + '/movies/load_preview_data',
				function ( data ) {
					$.each (data.query.results.div.div, function(i, item) {
						if ( item.a ) {
							current_date = item.a.content.split(' ')[1];
							$('div#moviepreview div.modal-body div.modal-moviepreview-info ul').append("<li class='separator'>"+ current_date +"</li>");
						}
						else {
							if ( item.div.class == 'wrapper_filme' ) {
								if ( item.div.div.length == 2 ) {
									$.each(item.div.div, function(y, item2) {
										$('div#moviepreview div.modal-body div.modal-moviepreview-info ul').append("<li><a href='#' data-preview-date='"+ current_date +"' data-value='"+ item2.div[1].h3.a.content.toLowerCase() +"'>"+ item2.div[1].h3.a.content +"</a></li>");
									});
								}
								else
									$('div#moviepreview div.modal-body div.modal-moviepreview-info ul').append("<li><a href='#' data-preview-date='"+ current_date +"' data-value='"+ item.div.div.div[1].h3.a.content.toLowerCase() +"'>"+ item.div.div.div[1].h3.a.content +"</a></li>");
							}
							else {
								$.each(item.div, function(j, item3) {
									$.each(item3.div, function(x, item4) {
										$('div#moviepreview div.modal-body div.modal-moviepreview-info ul').append("<li><a href='#' data-preview-date='"+ current_date +"' data-value='"+ item4.div[1].h3.a.content.toLowerCase() +"'>"+ item4.div[1].h3.a.content +"</a></li>");
									});
								});
							}
						}
					});

					// remove load
					field.css('background', 'none');

					Admin.modal_movie_preview();
				}
			)
		});
	},

	modal_movie_preview: function() {
		// open modal
		$('#moviepreview').modal({
			show: true
		});

		$('div.modal-moviepreview-info ul li a').unbind('click');
		$('div.modal-moviepreview-info ul li a').bind('click', function(e) {
			e.preventDefault();
			
			$('#moviepreview').modal('hide');
			$('input[name=release]').val($(this).data('preview-date'));
		});

		$('input#preview-filter').keyup( function(){
			var key = $(this).val();

			if ( key.length > 0 ) {
				$('div.modal-moviepreview-info ul li').hide();
				$("div.modal-moviepreview-info ul li a[data-value*='"+ key.toLowerCase() +"']").each(function(){
					$(this).parent().show();
					$("div.modal-moviepreview-info ul li.separator:contains('"+ $(this).data('preview-date') +"')").show();
				});
			}
			else 
				$('div.modal-moviepreview-info ul li').show();
		});
	},

	upload: function() {		
		 new AjaxUpload('uploadButton', {
            action: aura.path.approot + '/movies/upload/banner',
            name: 'file',
            onSubmit: function(file, extension) {
				$('#uploadButton').val('Carregando... Aguarde.').attr('disabled', 'disabled');
                    
				if (!Admin.upload_validate(extension, 'uploadButton'))
                    return false;

            	this.setData({ type: 'image'});
            },
            onComplete: function(file, response) {
				Admin.upload_on_complete('#uploadButton', 'banner[]', response);
            }
        });

        new AjaxUpload('uploadButton2', {
            action: aura.path.approot + '/movies/upload/thumb1',
            name: 'file',
            onSubmit: function(file, extension) {
				$('#uploadButton2').val('Carregando... Aguarde.').attr('disabled', 'disabled');
                    
				if (!Admin.upload_validate(extension, 'uploadButton2'))
                    return false;

            	this.setData({ type: 'image'});
            },
            onComplete: function(file, response) {
				Admin.upload_on_complete('#uploadButton2', 'thumb1', response);
            }
        });

        new AjaxUpload('uploadButton3', {
            action: aura.path.approot + '/movies/upload/thumb2',
            name: 'file',
            onSubmit: function(file, extension) {
				$('#uploadButton3').val('Carregando... Aguarde.').attr('disabled', 'disabled');
                   
                if (!Admin.upload_validate(extension, 'uploadButton3'))
                    return false;

            	this.setData({ type: 'image'});
            },
            onComplete: function(file, response) {
            	Admin.upload_on_complete('#uploadButton3', 'thumb2', response);
            }
        });

        new AjaxUpload('uploadButton4', {
            action: aura.path.approot + '/movies/upload/poster',
            name: 'file',
            onSubmit: function(file, extension) {
				$('#uploadButton4').val('Carregando... Aguarde.').attr('disabled', 'disabled');
                   
                if (!Admin.upload_validate(extension, 'uploadButton4'))
                    return false;

            	this.setData({ type: 'image'});
            },
            onComplete: function(file, response) {
            	Admin.upload_on_complete('#uploadButton4', 'poster', response);
            }
        });

        new AjaxUpload('uploadButton5', {
            action: aura.path.approot + '/movies/upload/facebook',
            name: 'file',
            onSubmit: function(file, extension) {
				$('#uploadButton5').val('Carregando... Aguarde.').attr('disabled', 'disabled');
                   
                if (!Admin.upload_validate(extension, 'uploadButton5'))
                    return false;

            	this.setData({ type: 'image'});
            },
            onComplete: function(file, response) {
            	Admin.upload_on_complete('#uploadButton5', 'facebook', response);
            }
        });
	},

	upload_validate: function( extension, button ) {
		if (!(extension && /^(jpg|png|jpeg|gif)$/i.test(extension))){
        	alert('Formato de imagem inválido! \n\t Tente novamente.');
            $(button).val('Carregar imagem').removeAttr('disabled');
            return false;
    	}

    	return true;
	},

	upload_on_complete: function(button, input_name, response) {
		if ( response != 'false' ) {
			var item = "<li>";
				item += "<a href='#' class='btn btn-danger' rel='remove-image'>&times;</a><br/>";
				item += "<img src='" + aura.path.media_files + "/movies/" + response + "' />";
				item += "<input type='hidden' name='" + input_name + "' value='" + response + "' />"
				item += "</li>";

			if ( input_name == 'banner[]' )
				$(button).parent().find('ul').append(item);
			else
				$(button).parent().find('ul').html(item);

			$(button).val('Carregar imagem').removeAttr('disabled');

			Admin.remove_image();
		}
	},

	remove_image: function() {
		$("a[rel='remove-image']").unbind('click');
		$("a[rel='remove-image']").bind('click', function(e){
			e.preventDefault();

			$(this).parent().remove();
		});
	},

	no_image: function() {
		$("a[rel='no-image']").click(function(e){
			e.preventDefault();

			var li = "<li>"+
						"<a href='#' class='btn btn-danger' rel='remove-image'>&times;</a><br/>"+
						"<img src='" + aura.web.img + "/" + $(this).attr('data-item') + "-no-image.jpg' alt='' />"+
						"<input type='hidden' name='" + $(this).attr('data-item-field') + "' value='poster-no-image.jpg' />"+
					"</li>";

			$(this).parent().find('ul').append(li);
		});
	},

	datepicker: function() {
		$( ".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'], monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'] });
	},

	revision: function() {
		$('input#revision').click(function(){
			if ( $('input[name=storyline_check]:checked').val() == 0 ) {
				alert( 'Aparentemente a sinopse não esta pronta!' );
				return false;
			}
		});
	}

}