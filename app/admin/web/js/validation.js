$(function(){
	Validation._init();
	Validation.mask();
});

Validation = {

	errors: '',
	
	_init: function() {
		$('form[data-validate=true]').submit(function() {
			Admin.errors = '';
			var error = false;
			Validation._check();

			$(this).find('[data-required=true]').each(function() {
				
				if ( !Validation._validate( $(this) ) )
					error = true ;

			});

			if ( $(this).find('[data-additional-elements-required]').length > 0 ) {
				var fields = $(this).find('[data-additional-elements-required]').attr('data-additional-elements-required').split(',');

				if ( !Validation._validate( $(this), true, fields ) )
					error = true ;
			}

			if ( error ) {
				Admin.alert_messages('error', Admin.errors);
				return false;
			}
		});

	},

	_check: function() {
		$('form[data-validate=true] [data-required=true]').blur(function(){
			Admin.errors = '';

			if ( Validation._validate( $(this) ) )
				$(this).removeClass('error');
		});
	},

	_validate: function( $this, verify_if_exists, fields ) {
		if ( verify_if_exists ) {
			var message = $this.find('[data-additional-elements-required]').attr('data-message');

			for (var i = 0; i<fields.length; i++) {
				if ( $this.find("[name='" + fields[i] + "']").length == 0 ) {
					Admin.errors += message + '<br/>';
					return false;
				}
			}

			return true;
		}

		var message = $this.attr('data-message');
			accept_value = $this.attr('data-accept-value');
			accept_value = accept_value !== undefined ? accept_value : false;

		if ($this.val() == '' || $this.val() == 0) {
			if ($this.val() !== accept_value) {
				Admin.errors += message + '<br/>';
				$this.addClass('error');
			}
		}

		if (Admin.errors != '')
			return false;
		else
			return true;
	},

	mask: function() {
		$('input[data-mask]').each(function() {
			$(this).mask($(this).attr('data-mask'));
		});
	}

}